﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ventasExpress
{
    public partial class CambiarContra : Form
    {
        Usuario user = new Usuario();
        public CambiarContra(string nombreUsuario)
        {
            InitializeComponent();
            lblNombreUsuario.Text = nombreUsuario;
        }

        private void btnGuardarContra_Click(object sender, EventArgs e)
        {
            if (txtCambiarContra.Text == "")
            {
                MessageBox.Show("Ingrese una contraseña");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea cambiar su contraseña?", "Cambiar contraseña", MessageBoxButtons.YesNo);
                
                if (dialogResult == DialogResult.Yes)
                {
                    if (lblNombreUsuario.Text == "admin")
                    {
                        Usuario.password[0] = user.Encriptar(txtCambiarContra.Text);
                    }
                    else if (lblNombreUsuario.Text == "vendedor")
                    {
                        Usuario.password[1] = user.Encriptar(txtCambiarContra.Text);
                    }
                    else
                    {
                        Usuario.password[2] = user.Encriptar(txtCambiarContra.Text);
                    }

                    MessageBox.Show("Su contraseña ha sido cambiada.\nRegresando al formulario de Login");

                    this.Close();
                    Program.login.Show();
                    Usuario.instanciaForm.Hide();
                }
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
