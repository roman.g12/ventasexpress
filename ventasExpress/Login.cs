﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ventasExpress
{
    public partial class Login : Form
    {
        int[] cantidad = new int[] { 30, 10, 50, 200, 500, 30, 25, 35, 200, 800 };
        string[] pass = new string[] { "240BE518FABD2724DDB6F04EEB1DA5967448D7E831C08C8FA822809F74C720A9", "930EE831FD5B2F806AFAC57C39E3A12F4699470C514D95DF4A19C039060C7F1D", "B2B3A0FEA1BECD9C9F73485A17FDFD039538965DA177334F226BAE07B9D32BD7" };
        Usuario user = new Usuario();

        SistemaVentas sis = new SistemaVentas();
        public Login()
        {
            InitializeComponent();
            Usuario.password = pass;
            Ventas.cantidades = cantidad;
        }

        private void lblIngresar_Click(object sender, EventArgs e)
        {
            //string usuario = txtUsuario.Text;
            if ((txtUsuario.Text == user.User[0] && user.Encriptar(txtPassword.Text) == Usuario.password[0].ToLower()) || (txtUsuario.Text == user.User[1] && user.Encriptar(txtPassword.Text) == Usuario.password[1].ToLower()) || (txtUsuario.Text == user.User[2] && user.Encriptar(txtPassword.Text) == Usuario.password[2].ToLower()))
            {
                //MessageBox.Show("Credenciales correctas");
                sis.lblUsuarioIngresado.Text = txtUsuario.Text;
                Usuario.instanciaForm = sis;
                this.Hide();
                Usuario.instanciaForm.ShowDialog();
                
                //sis.ShowDialog();
                //this.Close();
            }
            else
            {
                MessageBox.Show("Credenciales incorrectas");
            }
        }
    }
}
