﻿
namespace ventasExpress
{
    partial class VentaNueva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblIngresarVenta = new System.Windows.Forms.Label();
            this.txtIngresarProducto = new System.Windows.Forms.TextBox();
            this.dgvProductos = new System.Windows.Forms.DataGridView();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFacturar = new System.Windows.Forms.Button();
            this.btnProductosIngresados = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(888, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 25);
            this.btnSalir.TabIndex = 17;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblIngresarVenta
            // 
            this.lblIngresarVenta.AutoSize = true;
            this.lblIngresarVenta.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblIngresarVenta.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblIngresarVenta.Location = new System.Drawing.Point(401, 94);
            this.lblIngresarVenta.Name = "lblIngresarVenta";
            this.lblIngresarVenta.Size = new System.Drawing.Size(116, 17);
            this.lblIngresarVenta.TabIndex = 18;
            this.lblIngresarVenta.Text = "Ingresar venta:";
            // 
            // txtIngresarProducto
            // 
            this.txtIngresarProducto.Location = new System.Drawing.Point(401, 137);
            this.txtIngresarProducto.Name = "txtIngresarProducto";
            this.txtIngresarProducto.Size = new System.Drawing.Size(356, 23);
            this.txtIngresarProducto.TabIndex = 19;
            // 
            // dgvProductos
            // 
            this.dgvProductos.AllowUserToAddRows = false;
            this.dgvProductos.AllowUserToDeleteRows = false;
            this.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.Producto,
            this.Precio});
            this.dgvProductos.Location = new System.Drawing.Point(12, 58);
            this.dgvProductos.Name = "dgvProductos";
            this.dgvProductos.ReadOnly = true;
            this.dgvProductos.RowTemplate.Height = 25;
            this.dgvProductos.Size = new System.Drawing.Size(343, 324);
            this.dgvProductos.TabIndex = 20;
            // 
            // Codigo
            // 
            this.Codigo.HeaderText = "Código";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            // 
            // Producto
            // 
            this.Producto.HeaderText = "Producto";
            this.Producto.Name = "Producto";
            this.Producto.ReadOnly = true;
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio c/u";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            // 
            // btnFacturar
            // 
            this.btnFacturar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnFacturar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.btnFacturar.FlatAppearance.BorderSize = 0;
            this.btnFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFacturar.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnFacturar.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnFacturar.Location = new System.Drawing.Point(401, 322);
            this.btnFacturar.Name = "btnFacturar";
            this.btnFacturar.Size = new System.Drawing.Size(134, 60);
            this.btnFacturar.TabIndex = 21;
            this.btnFacturar.Text = "Facturar";
            this.btnFacturar.UseVisualStyleBackColor = false;
            this.btnFacturar.Click += new System.EventHandler(this.btnFacturar_Click);
            // 
            // btnProductosIngresados
            // 
            this.btnProductosIngresados.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnProductosIngresados.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.btnProductosIngresados.FlatAppearance.BorderSize = 0;
            this.btnProductosIngresados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductosIngresados.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnProductosIngresados.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnProductosIngresados.Location = new System.Drawing.Point(607, 322);
            this.btnProductosIngresados.Name = "btnProductosIngresados";
            this.btnProductosIngresados.Size = new System.Drawing.Size(150, 60);
            this.btnProductosIngresados.TabIndex = 22;
            this.btnProductosIngresados.Text = "Productos ingresados";
            this.btnProductosIngresados.UseVisualStyleBackColor = false;
            this.btnProductosIngresados.Visible = false;
            this.btnProductosIngresados.Click += new System.EventHandler(this.btnProductosIngresados_Click);
            // 
            // VentaNueva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(922, 497);
            this.Controls.Add(this.btnProductosIngresados);
            this.Controls.Add(this.btnFacturar);
            this.Controls.Add(this.dgvProductos);
            this.Controls.Add(this.txtIngresarProducto);
            this.Controls.Add(this.lblIngresarVenta);
            this.Controls.Add(this.btnSalir);
            this.Name = "VentaNueva";
            this.Text = "VentaNueva";
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblIngresarVenta;
        private System.Windows.Forms.TextBox txtIngresarProducto;
        private System.Windows.Forms.DataGridView dgvProductos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.Button btnFacturar;
        private System.Windows.Forms.Button btnProductosIngresados;
    }
}