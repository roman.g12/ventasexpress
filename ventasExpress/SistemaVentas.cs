﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ventasExpress
{
    public partial class SistemaVentas : Form
    {
        //Le pasamos las cantidades al arreglo de cantidad de nuestra clase de Ventas para poder manejar las cantidades de los productos que se venden que todos los formularios
        //int[] cantidad = new int[] { 30, 10, 50, 200, 500, 30, 25, 35, 200, 800 };
        Ventas ventas = new Ventas();

        public SistemaVentas()
        {
            InitializeComponent();
            //Ventas.cantidades = cantidad;
            //ventas.asignarCantidad(cantidad);
        }

        //Método para abrir los formularios en el panel
        private Form formularioAbierto = null;
        private void abrirFormularios(Form formulario)
        {
            if (formularioAbierto != null)
            {
                formularioAbierto.Close();
            }
            formularioAbierto = formulario;
            formulario.TopLevel = false; //Indicamos que el formulario se comportará como un control
            formulario.FormBorderStyle = FormBorderStyle.None;
            formulario.Dock = DockStyle.Fill;
            pnlContenedor.Controls.Add(formulario); //Agregamos el formulario a la lista de controles de pnlContenedor
            pnlContenedor.Tag = formulario; //Asociamos el formulario con nuestro panel
            //formulario.BringToFront(); //Por si usamos un logo en el panel
            formulario.Show(); //Mostramos el formulario
        }

        private void btnVentaNueva_Click(object sender, EventArgs e)
        {
            abrirFormularios(new VentaNueva());
        }

        private void btnConsultarInventarios_Click(object sender, EventArgs e)
        {
            abrirFormularios(new ConsultarInventarios());
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            MessageBox.Show("HASTA LA PRÓXIMA");
            this.Close();
            Program.login.Close();
        }

        private void btnCambiarContra_Click(object sender, EventArgs e)
        {
            string nombreUser = lblUsuarioIngresado.Text;
            abrirFormularios(new CambiarContra(nombreUser));
        }
    }
}
