﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ventasExpress
{
    public partial class Facturar : Form
    {
        public string[] producto = new string[] { };
        public int[] cantidad;
        public double[] precio;
        public int contador = 0;

        public Facturar(string[] producto, int[] cantidad, double[] precio, int contador, bool banderaMensaje)
        {
            InitializeComponent();

            this.producto = producto;
            this.cantidad = cantidad;
            this.precio = precio;
            this.contador = contador;

            //MessageBox.Show(algo);

            llenarLst(banderaMensaje);
        }

        private void llenarLst(bool banderaMensaje)
        {
            int contadorCriterio = 0;
            double suma = 0;
            lstDetalle.Items.Clear();
            lstDetalle.Items.Add("Factura de compra");
            lstDetalle.Items.Add("Supermercado Don Diego");
            lstDetalle.Items.Add("**********************************************");
            for (int i = 0; i < contador; i++)
            {
                suma += cantidad[i] * precio[i];
                lstDetalle.Items.Add(producto[i] + " ---- " + cantidad[i].ToString() + " x " + " $" + precio[i] + " = " + Math.Round(cantidad[i]*precio[i], 2));

                if (producto[i] == "Pollo" && cantidad[i] == 1) { contadorCriterio++; }
                if (producto[i] == "Gaseosa" && cantidad[i] == 1) { contadorCriterio++; }
            }
            if (contadorCriterio == 2)
            {
                lstDetalle.Items.Add("Regalo de la tienda: Dulces ---- 5 x $0.00" );
                Ventas.cantidades[4] -= 5;
            }
            lstDetalle.Items.Add("----------");

            if (suma > 20)
            {
                suma -= suma * 0.03;
                lstDetalle.Items.Add("Por cortesía de la tienda se ");
                lstDetalle.Items.Add("le ha aplicado un descuento del 3%");
                lstDetalle.Items.Add("Total de factura: $" + Math.Round(suma, 2));
            }
            else
            {
                lstDetalle.Items.Add("Total de factura: $" + Math.Round(suma, 2));
            }

            if (banderaMensaje == false)
            {
                lstDetalle.Items.Add("Lo lamentamos pero no poseemos existencias de algunos productos:");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
