﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace ventasExpress
{
    public class Usuario
    {
        string[] user;
        //string [] password;

        static public string[] password;
        static public Form instanciaForm;

        public Usuario()
        {
            user = new string[] { "admin", "vendedor", "invitado" };
            //this.password = password;
            //this.password = new string[] { "240BE518FABD2724DDB6F04EEB1DA5967448D7E831C08C8FA822809F74C720A9", "930EE831FD5B2F806AFAC57C39E3A12F4699470C514D95DF4A19C039060C7F1D", "B2B3A0FEA1BECD9C9F73485A17FDFD039538965DA177334F226BAE07B9D32BD7" };
        }

        //Método encargado de encriptar la contraseña
        public string Encriptar(string password)
        {
            SHA256 sha256 = SHA256Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha256.ComputeHash(encoding.GetBytes(password));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        public string[] User { get => user; set => user = value; }
        //public string[] Password { get => password; set => password = value; }
    }
}
