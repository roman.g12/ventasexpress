﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using static System.Windows.Forms.Design.AxImporter;

namespace ventasExpress
{
    public partial class VentaNueva : Form
    {
        //public int[] cantidades = new int[] { 30, 5, 50, 200, 500, 30, 25, 35, 200, 800 };

        Ventas ve = new Ventas();
        //Creamos la matriz donde guardaremos nuestro inventario
        //public string[,] productos = new string[3, 10];
        public VentaNueva()
        {
            InitializeComponent();
            llenarDataGrid();

            //this.cantidades = cantidades;
        }

        private void llenarDataGrid()
        {
            dgvProductos.Rows.Add(10);
            dgvProductos.Rows[0].Cells[0].Value = "1-";
            dgvProductos.Rows[0].Cells[1].Value = "Huevos";
            dgvProductos.Rows[0].Cells[2].Value = "$0.10";

            dgvProductos.Rows[1].Cells[0].Value = "2-";
            dgvProductos.Rows[1].Cells[1].Value = "Pollo";
            dgvProductos.Rows[1].Cells[2].Value = "$5.00";

            dgvProductos.Rows[2].Cells[0].Value = "3-";
            dgvProductos.Rows[2].Cells[1].Value = "Aceite";
            dgvProductos.Rows[2].Cells[2].Value = "$3.00";

            dgvProductos.Rows[3].Cells[0].Value = "4-";
            dgvProductos.Rows[3].Cells[1].Value = "Fósforos";
            dgvProductos.Rows[3].Cells[2].Value = "$0.50";

            dgvProductos.Rows[4].Cells[0].Value = "5-";
            dgvProductos.Rows[4].Cells[1].Value = "Dulces";
            dgvProductos.Rows[4].Cells[2].Value = "$0.80";

            dgvProductos.Rows[5].Cells[0].Value = "6-";
            dgvProductos.Rows[5].Cells[1].Value = "Margarina";
            dgvProductos.Rows[5].Cells[2].Value = "$0.30";

            dgvProductos.Rows[6].Cells[0].Value = "7-";
            dgvProductos.Rows[6].Cells[1].Value = "Jabón";
            dgvProductos.Rows[6].Cells[2].Value = "$2.25";

            dgvProductos.Rows[7].Cells[0].Value = "8-";
            dgvProductos.Rows[7].Cells[1].Value = "Carne";
            dgvProductos.Rows[7].Cells[2].Value = "$2.75";

            dgvProductos.Rows[8].Cells[0].Value = "9-";
            dgvProductos.Rows[8].Cells[1].Value = "Gaseosa";
            dgvProductos.Rows[8].Cells[2].Value = "$1.80";

            dgvProductos.Rows[9].Cells[0].Value = "10-";
            dgvProductos.Rows[9].Cells[1].Value = "Desechables";
            dgvProductos.Rows[9].Cells[2].Value = "$3.25";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProductosIngresados_Click(object sender, EventArgs e)
        {
            if (txtIngresarProducto.Text == "")
            {
                MessageBox.Show("No se a ingresado ningún dato");
            }
            else
            {
                //Evaluaremos los números ingresados en la txt y lo comparemos con cada producto registrado en la clase
                string[] datos = txtIngresarProducto.Text.Split(","); //Obtenemos los números ingresados en la ventas
                
                //Veremos los datos y las evaluaciones como si estuvieran en un arreglo de 2 dimensiones.
                /*El primer for será para medir la posición de de los números y determinar si es cantidad o código de producto también servirá para comparar el dato con todos
                los códigos de los productos*/
                /*for (int i = 0; i < datos.Length; i++) { 
                    //El segundo for servirá para recorrer cada código y compararlo con los números ingresados en la txt.
                    for (int j = 0; j <= 9; j++)
                    {
                        //MessageBox.Show("Imprimiendi i " + i);
                        if (i % 2 == 0)
                        {
                            if (datos[i].Equals(ve.Codigo[j].ToString()))
                            {
                                //MessageBox.Show("entre a producto");
                                productos[contador] = ve.Producto[j];
                                precio[contador] = ve.Precio[j];
                                contador++;
                            }
                        }
                        else
                        {
                            if (datos[i].Equals(ve.Codigo[j].ToString()))
                            {
                                //MessageBox.Show("entre a cantidad");
                                cantidad[contador2] = Int32.Parse(datos[i]);
                                contador2++;
                            }
                        }
                        //MessageBox.Show(ve.Codigo[j].ToString());
                    }
                }

                for(int i = 0; i < contador; i++)
                {
                    listaProducto.Add(productos[i]);
                    listaCantidad.Add(cantidad[i]);
                }*/
            }
        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            bool banderaMensaje = true, bandera2 = true, banderaDatosErroneos = true;
            string[] productosSinExistencias = new string[10];
            string[] productos = new string[10];
            int[] cantidadIngresada = new int[10];
            double[] precio = new double[10];
            int contador = 0, contador2 = 0, contador3 = 0;

            DialogResult dialogResult = MessageBox.Show("Está seguro que desea realizar la factura", "Realizar factura", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                if (txtIngresarProducto.Text == "")
                {
                    MessageBox.Show("No se a ingresado ningún dato");
                }
                else
                {
                    //Evaluaremos los números ingresados en la txt y lo comparemos con cada producto registrado en la clase
                    string[] datos = txtIngresarProducto.Text.Split(","); //Obtenemos los números ingresados en la ventas

                    for (int i = 0; i < datos.Length; i++)
                    {
                        if (datos[i] == "")
                        {
                            banderaDatosErroneos = false;
                        }
                    }

                    //validamos que ingrese la cantida de cada producto
                    if (datos.Length % 2 == 0 && banderaDatosErroneos != false)
                    {
                        //Veremos los datos y las evaluaciones como si estuvieran en un arreglo de 2 dimensiones.
                        /*El primer for será para medir la posición de de los números y determinar si es cantidad o código de producto también servirá para comparar el dato con todos
                        los códigos de los productos*/
                        for (int i = 0; i < datos.Length; i++)
                        {
                            //El segundo for servirá para recorrer cada código y compararlo con los números ingresados en la txt.
                            for (int j = 0; j <= 9; j++)
                            {
                                //MessageBox.Show(cantidades[j].ToString());
                                if (i % 2 == 0)
                                {
                                    if (datos[i].Equals(ve.Codigo[j].ToString()))
                                    {
                                        if (Ventas.cantidades[j] > 0) //Validamos que haya existencias, si no hay evitamos registrar el producto en la factura y mostramos el mensaje de disculpas
                                        {
                                            productos[contador] = ve.Producto[j];
                                            precio[contador] = ve.Precio[j];
                                            contador++;
                                        }
                                        else
                                        {
                                            banderaMensaje = false;
                                            bandera2 = false;
                                            productosSinExistencias[contador3] = ve.Producto[j]; //Arreglo que mostrará los productos que no tienen existencias en la factura
                                        }

                                        //Validamos si la cantidad ingresada es menor a la cantidad registrada para poder hacer la resta o cambiar la cantidad ingresada por la registrada
                                        if (Ventas.cantidades[j] < Int32.Parse(datos[i+1]) && Ventas.cantidades[j] > 0)
                                        {
                                            if (bandera2 != false)
                                            {
                                                cantidadIngresada[contador2] = Ventas.cantidades[j];
                                                Ventas.cantidades[j] -= Ventas.cantidades[j];
                                                contador2++;
                                            }
                                            bandera2 = true;
                                        }
                                        else
                                        {
                                            if (bandera2 != false)
                                            {
                                                Ventas.cantidades[j] -= Int32.Parse(datos[i+1]);

                                                cantidadIngresada[contador2] = Int32.Parse(datos[i+1]);
                                                contador2++;
                                            }
                                            bandera2 = true;
                                        }
                                    }
                                }
                            }
                        }
                        txtIngresarProducto.Clear();

                        Facturar frmFacturar = new Facturar(productos, cantidadIngresada, precio, contador, banderaMensaje);
                        frmFacturar.Show();
                    }
                    else
                    {
                        MessageBox.Show("Favor revisar que se haya ingresado la venta correctamente");
                    }
                }
            }
            //Enviamos las cantidades actualizadas al formulario de inventario
            //Ventas.cantidades = cantidades;
        }
    }
}
