﻿
namespace ventasExpress
{
    partial class CambiarContra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTituloCambiarContra = new System.Windows.Forms.Label();
            this.lblNombreUsuario = new System.Windows.Forms.Label();
            this.txtCambiarContra = new System.Windows.Forms.TextBox();
            this.lblFavorIngresar = new System.Windows.Forms.Label();
            this.btnGuardarContra = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTituloCambiarContra
            // 
            this.lblTituloCambiarContra.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTituloCambiarContra.AutoSize = true;
            this.lblTituloCambiarContra.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTituloCambiarContra.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblTituloCambiarContra.Location = new System.Drawing.Point(180, 95);
            this.lblTituloCambiarContra.Name = "lblTituloCambiarContra";
            this.lblTituloCambiarContra.Size = new System.Drawing.Size(396, 37);
            this.lblTituloCambiarContra.TabIndex = 2;
            this.lblTituloCambiarContra.Text = "Cambiar contraseña usuario:";
            // 
            // lblNombreUsuario
            // 
            this.lblNombreUsuario.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNombreUsuario.AutoSize = true;
            this.lblNombreUsuario.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblNombreUsuario.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblNombreUsuario.Location = new System.Drawing.Point(582, 95);
            this.lblNombreUsuario.Name = "lblNombreUsuario";
            this.lblNombreUsuario.Size = new System.Drawing.Size(121, 37);
            this.lblNombreUsuario.TabIndex = 3;
            this.lblNombreUsuario.Text = "nombre";
            // 
            // txtCambiarContra
            // 
            this.txtCambiarContra.Location = new System.Drawing.Point(127, 273);
            this.txtCambiarContra.Name = "txtCambiarContra";
            this.txtCambiarContra.PasswordChar = '*';
            this.txtCambiarContra.Size = new System.Drawing.Size(314, 23);
            this.txtCambiarContra.TabIndex = 4;
            // 
            // lblFavorIngresar
            // 
            this.lblFavorIngresar.AutoSize = true;
            this.lblFavorIngresar.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblFavorIngresar.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblFavorIngresar.Location = new System.Drawing.Point(127, 228);
            this.lblFavorIngresar.Name = "lblFavorIngresar";
            this.lblFavorIngresar.Size = new System.Drawing.Size(205, 17);
            this.lblFavorIngresar.TabIndex = 5;
            this.lblFavorIngresar.Text = "Ingrese su nueva cotraseña";
            // 
            // btnGuardarContra
            // 
            this.btnGuardarContra.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnGuardarContra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.btnGuardarContra.FlatAppearance.BorderSize = 0;
            this.btnGuardarContra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarContra.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnGuardarContra.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnGuardarContra.Location = new System.Drawing.Point(603, 255);
            this.btnGuardarContra.Name = "btnGuardarContra";
            this.btnGuardarContra.Size = new System.Drawing.Size(134, 41);
            this.btnGuardarContra.TabIndex = 14;
            this.btnGuardarContra.Text = "Cambiar";
            this.btnGuardarContra.UseVisualStyleBackColor = false;
            this.btnGuardarContra.Click += new System.EventHandler(this.btnGuardarContra_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(855, 26);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(22, 25);
            this.btnSalir.TabIndex = 18;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // CambiarContra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(941, 482);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnGuardarContra);
            this.Controls.Add(this.lblFavorIngresar);
            this.Controls.Add(this.txtCambiarContra);
            this.Controls.Add(this.lblNombreUsuario);
            this.Controls.Add(this.lblTituloCambiarContra);
            this.Name = "CambiarContra";
            this.Text = "CambiarContra";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTituloCambiarContra;
        private System.Windows.Forms.TextBox txtCambiarContra;
        private System.Windows.Forms.Label lblFavorIngresar;
        private System.Windows.Forms.Button btnGuardarContra;
        public System.Windows.Forms.Label lblNombreUsuario;
        private System.Windows.Forms.Button btnSalir;
    }
}