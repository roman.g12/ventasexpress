﻿
namespace ventasExpress
{
    partial class SistemaVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnCambiarContra = new System.Windows.Forms.Button();
            this.btnConsultarInventarios = new System.Windows.Forms.Button();
            this.btnVentaNueva = new System.Windows.Forms.Button();
            this.pnlBienvenida = new System.Windows.Forms.Panel();
            this.lblUsuarioIngresado = new System.Windows.Forms.Label();
            this.lblBienvenido = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.pnlMenu.SuspendLayout();
            this.pnlBienvenida.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.pnlMenu.Controls.Add(this.btnSalir);
            this.pnlMenu.Controls.Add(this.btnCambiarContra);
            this.pnlMenu.Controls.Add(this.btnConsultarInventarios);
            this.pnlMenu.Controls.Add(this.btnVentaNueva);
            this.pnlMenu.Controls.Add(this.pnlBienvenida);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(941, 94);
            this.pnlMenu.TabIndex = 0;
            // 
            // btnSalir
            // 
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Britannic Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnSalir.Location = new System.Drawing.Point(509, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnSalir.Size = new System.Drawing.Size(181, 94);
            this.btnSalir.TabIndex = 6;
            this.btnSalir.Text = "Salir del sistema";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnCambiarContra
            // 
            this.btnCambiarContra.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCambiarContra.FlatAppearance.BorderSize = 0;
            this.btnCambiarContra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCambiarContra.Font = new System.Drawing.Font("Britannic Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnCambiarContra.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnCambiarContra.Location = new System.Drawing.Point(337, 0);
            this.btnCambiarContra.Name = "btnCambiarContra";
            this.btnCambiarContra.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnCambiarContra.Size = new System.Drawing.Size(172, 94);
            this.btnCambiarContra.TabIndex = 4;
            this.btnCambiarContra.Text = "Cambiar contraseña";
            this.btnCambiarContra.UseVisualStyleBackColor = true;
            this.btnCambiarContra.Click += new System.EventHandler(this.btnCambiarContra_Click);
            // 
            // btnConsultarInventarios
            // 
            this.btnConsultarInventarios.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnConsultarInventarios.FlatAppearance.BorderSize = 0;
            this.btnConsultarInventarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultarInventarios.Font = new System.Drawing.Font("Britannic Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnConsultarInventarios.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnConsultarInventarios.Location = new System.Drawing.Point(147, 0);
            this.btnConsultarInventarios.Name = "btnConsultarInventarios";
            this.btnConsultarInventarios.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnConsultarInventarios.Size = new System.Drawing.Size(190, 94);
            this.btnConsultarInventarios.TabIndex = 3;
            this.btnConsultarInventarios.Text = "Consultar inventarios";
            this.btnConsultarInventarios.UseVisualStyleBackColor = true;
            this.btnConsultarInventarios.Click += new System.EventHandler(this.btnConsultarInventarios_Click);
            // 
            // btnVentaNueva
            // 
            this.btnVentaNueva.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnVentaNueva.FlatAppearance.BorderSize = 0;
            this.btnVentaNueva.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVentaNueva.Font = new System.Drawing.Font("Britannic Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnVentaNueva.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnVentaNueva.Location = new System.Drawing.Point(0, 0);
            this.btnVentaNueva.Name = "btnVentaNueva";
            this.btnVentaNueva.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnVentaNueva.Size = new System.Drawing.Size(147, 94);
            this.btnVentaNueva.TabIndex = 2;
            this.btnVentaNueva.Text = "Venta nueva";
            this.btnVentaNueva.UseVisualStyleBackColor = true;
            this.btnVentaNueva.Click += new System.EventHandler(this.btnVentaNueva_Click);
            // 
            // pnlBienvenida
            // 
            this.pnlBienvenida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(11)))));
            this.pnlBienvenida.Controls.Add(this.lblUsuarioIngresado);
            this.pnlBienvenida.Controls.Add(this.lblBienvenido);
            this.pnlBienvenida.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlBienvenida.Location = new System.Drawing.Point(714, 0);
            this.pnlBienvenida.Name = "pnlBienvenida";
            this.pnlBienvenida.Size = new System.Drawing.Size(227, 94);
            this.pnlBienvenida.TabIndex = 0;
            // 
            // lblUsuarioIngresado
            // 
            this.lblUsuarioIngresado.AutoSize = true;
            this.lblUsuarioIngresado.Font = new System.Drawing.Font("Britannic Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblUsuarioIngresado.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblUsuarioIngresado.Location = new System.Drawing.Point(71, 52);
            this.lblUsuarioIngresado.Name = "lblUsuarioIngresado";
            this.lblUsuarioIngresado.Size = new System.Drawing.Size(74, 21);
            this.lblUsuarioIngresado.TabIndex = 1;
            this.lblUsuarioIngresado.Text = "nombre";
            // 
            // lblBienvenido
            // 
            this.lblBienvenido.AutoSize = true;
            this.lblBienvenido.Font = new System.Drawing.Font("Britannic Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblBienvenido.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblBienvenido.Location = new System.Drawing.Point(54, 19);
            this.lblBienvenido.Name = "lblBienvenido";
            this.lblBienvenido.Size = new System.Drawing.Size(115, 21);
            this.lblBienvenido.TabIndex = 0;
            this.lblBienvenido.Text = "BIENVENIDO:";
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 94);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(941, 15);
            this.panel1.TabIndex = 1;
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 109);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(941, 482);
            this.pnlContenedor.TabIndex = 2;
            // 
            // SistemaVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(941, 591);
            this.ControlBox = false;
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlMenu);
            this.Name = "SistemaVentas";
            this.Text = "SistemaVentas";
            this.pnlMenu.ResumeLayout(false);
            this.pnlBienvenida.ResumeLayout(false);
            this.pnlBienvenida.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnConsultarInventarios;
        private System.Windows.Forms.Button btnVentaNueva;
        private System.Windows.Forms.Panel pnlBienvenida;
        private System.Windows.Forms.Label lblBienvenido;
        public System.Windows.Forms.Label lblUsuarioIngresado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnCambiarContra;
    }
}