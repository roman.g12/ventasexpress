﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ventasExpress
{
    public class Ventas
    {
        int[] codigo;
        string[] producto;
        int[] cantidad;
        double[] precio;

        static public int[] cantidades;

        public Ventas()
        {
            codigo = new int[] { 1,2,3,4,5,6,7,8,9,10 };
            producto = new string[] { "Huevos", "Pollo", "Aceite", "Fósforos", "Dulces", "Margarina", "Jabón", "Carne", "Gaseosa", "Desechables" };
            Precio = new double[] { 0.10, 5.00, 3.00, 0.50, 0.80, 0.30, 2.25, 2.75, 1.80, 3.25 };
            //cantidad = new int[] { 30, 10, 50, 200, 500, 30, 25, 35, 200, 800};
        }

        public void asignarCantidad(int[] cantidad)
        {
            this.cantidad = cantidad;
        }

        //Método que se encargará de restar la cantidad de productos que hay cuando se realiza una venta
        public void ModificarCantidad(int resta, int indice)
        {
            //this.cantidad[indice] -= resta;
        }

        public int[] Codigo { get => codigo; set => codigo = value; }
        public string[] Producto { get => producto; set => producto = value; }
        public int[] Cantidad { get => cantidad; set => cantidad = value; }
        public double[] Precio { get => precio; set => precio = value; }
    }
}
