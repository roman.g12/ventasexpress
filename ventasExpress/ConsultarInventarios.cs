﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ventasExpress
{
    public partial class ConsultarInventarios : Form
    {
        //private List<Ventas> productosFiltrados = new List<Ventas>();
        Ventas ventas = new Ventas();
        public ConsultarInventarios()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            int codigo = 0, cantidad = 0;
            string nombreProducto = "sin asignar";

            if (txtFiltrar.Text.ToUpper() == "TODOS" || txtFiltrar.Text.ToUpper() == "TODO")
            {
                dgvProductos.Rows.Clear();
                for (int i = 0; i < 10; i++)
                {
                    dgvProductos.Rows.Add(1);
                    dgvProductos.Rows[i].Cells[0].Value = ventas.Codigo[i];
                    dgvProductos.Rows[i].Cells[1].Value = ventas.Producto[i];
                    dgvProductos.Rows[i].Cells[2].Value = Ventas.cantidades[i];
                }
            }
            else if (txtFiltrar.Text == "")
            {
                dgvProductos.Rows.Clear();
            }
            else
            {
                dgvProductos.Rows.Clear();
                for (int i = 0; i < 10; i++)
                {
                    if (ventas.Producto[i].ToUpper().Contains(txtFiltrar.Text.ToUpper().ToString()))
                    {
                        nombreProducto = ventas.Producto[i];
                        codigo = ventas.Codigo[i];
                        cantidad = Ventas.cantidades[i];
                    }
                    else if (ventas.Codigo[i].ToString().Contains(txtFiltrar.Text))
                    {
                        nombreProducto = ventas.Producto[i];
                        codigo = ventas.Codigo[i];
                        cantidad = Ventas.cantidades[i];
                    }
                }

                dgvProductos.Rows.Add(1);
                dgvProductos.Rows[0].Cells[0].Value = codigo;
                dgvProductos.Rows[0].Cells[1].Value = nombreProducto;
                dgvProductos.Rows[0].Cells[2].Value = cantidad;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
